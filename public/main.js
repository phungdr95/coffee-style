
const topMenu = document.getElementById('htp-top-menu')
const toggleTopMenuIcon = document.getElementById('htp-toggle-top-menu-icon')

document.addEventListener('click', (e) => {
    if (toggleTopMenuIcon.contains(e.target)) {
        // Click to Toggle top menu icon
        topMenu.classList.toggle('htp-top-menu-expanded')
        topMenu.classList.toggle('hidden')
    } else {
        //Click Outside from Toggle top menu icon
        if (topMenu.classList.contains('htp-top-menu-expanded')) {
            topMenu.classList.remove('htp-top-menu-expanded')
            topMenu.classList.add('hidden')
        }
    }
})